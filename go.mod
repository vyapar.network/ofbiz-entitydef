module ofbiz

go 1.18

require github.com/basgys/goxml2json v1.1.0

require (
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/text v0.3.7 // indirect
)
