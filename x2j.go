package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	xj "github.com/basgys/goxml2json"
)

var xmlFile, jsonFile = "", ""

func init() {
	flag.StringVar(&xmlFile, "xml", "", "input XML entity def")
	flag.StringVar(&jsonFile, "json", "", "JSON file output")
}

func main() {
	flag.Parse()
	if xmlFile == "" {
		fmt.Println("xml inputfile not provided")
		flag.PrintDefaults()
		os.Exit(1)
	}
	// xml is an io.Reader
	xml, err := os.Open(xmlFile) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	json, err := xj.Convert(xml)
	if err != nil {
		panic("That's embarrassing...")
	}
	if jsonFile == "" {
		if n := strings.LastIndex(xmlFile, "."); n != -1 {
			fname := xmlFile[:n]
			jsonFile = fname + ".json"
		} else {
			jsonFile = "out.json"
		}

	}
	err = os.WriteFile(jsonFile, json.Bytes(), 0666)
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("JSON output", jsonFile)
	}

}
